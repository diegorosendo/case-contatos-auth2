package br.com.itau.contatos.service;

import br.com.itau.contatos.models.Contato;
import br.com.itau.contatos.repository.ContatoRepository;
import br.com.itau.contatos.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    ContatoRepository contatoRepository;

    public Contato create(Contato contato, Usuario usuario){
        contato.setOwnerId(usuario.getId());
        return contatoRepository.save(contato);
    }

    public List<Contato> getAllByOwnerId(int ownerId){
        return contatoRepository.findAllByOwnerId(ownerId);
    }

}
