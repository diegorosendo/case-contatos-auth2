package br.com.itau.contatos.controller;

import br.com.itau.contatos.models.Contato;
import br.com.itau.contatos.security.Usuario;
import br.com.itau.contatos.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/contato")
public class ContatoController {

//    @GetMapping("/{tipo}")
//    public Contato getInstrumentoMusical(@PathVariable("tipo") String tipo, Principal principal) {
//        Contato contato = new Contato();
//        contato.setTipo(tipo);
//        contato.setDono(principal.getName());
//        return contato;
//    }


    @Autowired
    ContatoService contatoService;

    @PostMapping
    public Contato create(@RequestBody Contato contato, @AuthenticationPrincipal Usuario usuario) {

        return contatoService.create(contato, usuario);

    }

    @GetMapping
    public List<Contato> view(@AuthenticationPrincipal Usuario usuario) {

        return contatoService.getAllByOwnerId((int)usuario.getId());

    }


}
