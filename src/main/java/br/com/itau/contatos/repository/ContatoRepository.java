package br.com.itau.contatos.repository;

import br.com.itau.contatos.models.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository  extends CrudRepository<Contato, Long> {

    List<Contato> findAllByOwnerId(int onwerId);

}
